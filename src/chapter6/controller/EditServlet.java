package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns= {"/edit"})
public class EditServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String regex = "^[1-9][0-9]*";
		String editId =  request.getParameter("edit");
		Message message = null;

		List<String> errorMessages = new ArrayList<>();

		if (!StringUtils.isBlank(editId) && editId.matches(regex)) {
			int messageId = Integer.parseInt(editId);
			message = new MessageService().select(messageId);
		}

		if (message== null) {
			errorMessages.add("不正なパラメータが入力されました");
			request.getSession().setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		request.setAttribute("message", message);
		request.getRequestDispatcher("/edit.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String text = request.getParameter("text");
		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("messageId")));
		message.setText(text);
		List<String> errorMessages = new ArrayList<>();
		if(!isValid(text,errorMessages)) {
			request.getSession().setAttribute("errorMessages", errorMessages);
			request.getSession().setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}
		new MessageService().update(message);
		response.sendRedirect("./");
	}

	private boolean isValid(String message, List<String> errorMessages) {

		if(StringUtils.isBlank(message)) {
			errorMessages.add("メッセージを入力してください");
		}else if(140< message.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if(errorMessages.size() !=0) {
			return false;
		}
		return true;
	}
}
