package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Comment;
import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;
public class CommentDao {

	public void insert(Connection connection , Comment comment) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" INSERT INTO comments ( ");
			sql.append(" text, ");
			sql.append(" user_id, ");
			sql.append(" message_id, ");
			sql.append(" created_date, ");
			sql.append(" updated_date ");
			sql.append(" ) VALUES ( ");
			sql.append(" ?, ");
			sql.append(" ?, ");
			sql.append(" ?, ");
			sql.append(" CURRENT_TIMESTAMP, ");
			sql.append(" CURRENT_TIMESTAMP ");
			sql.append(" ) ");
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getMessageId());
			ps.executeUpdate();
		}catch(SQLException e ) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}
	public List<UserComment> select(Connection connection){
		PreparedStatement ps = null;
		try {
			ResultSet rs = null;
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT ");
			sql.append(" comments.id AS id, ");
			sql.append(" comments.text AS text, ");
			sql.append(" comments.user_id AS user_id, ");
			sql.append(" comments.message_id AS message_id, ");
			sql.append(" users.account AS account, ");
			sql.append(" users.name AS name, ");
			sql.append(" comments.created_date AS created_date ");
			sql.append(" FROM comments ");
			sql.append(" INNER JOIN users ");
			sql.append(" ON user_id = users.id ");
			ps = connection.prepareStatement(sql.toString());
			rs = ps.executeQuery();
			List<UserComment> list = toUserComments(rs);
			return list;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<UserComment> toUserComments(ResultSet rs) throws SQLException{

		List<UserComment> comments = new ArrayList<>();
		try {
			while(rs.next()) {
				UserComment comment = new UserComment();
				comment.setId(rs.getInt("id"));
				comment.setComment(rs.getString("text"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setMessageId(rs.getInt("message_id"));
				comment.setAccount(rs.getString("account"));
				comment.setName(rs.getString("name"));
				comment.setCreatedDate(rs.getTimestamp("created_date"));
				comments.add(comment);
			}
			return comments;
		}finally {
			close(rs);
		}
	}
}
