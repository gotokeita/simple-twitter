package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection , Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" INSERT INTO messages ( ");
			sql.append(" user_id, ");
			sql.append(" text, ");
			sql.append(" created_date, ");
			sql.append(" updated_date ");
			sql.append(" ) VALUES ( ");
			sql.append(" ?, ");
			sql.append(" ?, ");
			sql.append(" CURRENT_TIMESTAMP, ");
			sql.append(" CURRENT_TIMESTAMP ");
			sql.append(" ) ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getText());

			ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}
	public void delete(Connection connection , int id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" DELETE FROM messages ");
			sql.append(" WHERE id = ? ");
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void upDate(Connection connection , String message, int messageId)  {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" UPDATE MESSAGES  ");
			sql.append(" SET TEXT = ? ");
			sql.append(" WHERE id = ? ");
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, message);
			ps.setInt(2, messageId);
			ps.executeUpdate();
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}finally {
			close(ps);
		}
	}

	//指定のメッセージを1件取得
	public Message select(Connection connection, int messageId) {
		PreparedStatement ps = null;
		List<Message>messages = new ArrayList<Message>();
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT * FROM messages ");
			sql.append(" WHERE id = ? ");
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, messageId);
			ResultSet rs = ps.executeQuery();
			messages = toMessage(rs);
			if(messages.isEmpty()) {
				return null;
			}else if(2<=messages.size()) {
				throw new SQLException("メッセージが重複しています");
			}else {
				return messages.get(0);
			}

		}catch(SQLException e) {
			throw new RuntimeException(e);
		}
		finally {
			close(ps);
		}
	}

	private List<Message> toMessage(ResultSet rs) throws SQLException {
		List<Message> messages = new ArrayList<Message>();
		try {
			while(rs.next()) {
				Message message = new Message();
				message.setId(rs.getInt("id"));
				message.setUserId(rs.getInt("user_id"));
				message.setText(rs.getString("text"));
				message.setUpdatedDate(rs.getTimestamp("created_date"));
				message.setUpdatedDate(rs.getTimestamp("updated_date"));
				messages.add(message);
			}
			return messages;
		}finally {
			rs.close();
		}
	}
}
