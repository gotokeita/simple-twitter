package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;
public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection,message);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId , String start, String end){
		final int LIMIT_NUM = 1000;
		Connection connection = null;

		try {
			connection = getConnection();
			Integer id = null;
			if(userId !=null) {
				id = Integer.parseInt(userId);
			}

			if(!StringUtils.isEmpty(start)) {
				start += " 00:00:00";
			}else {
				start = "2020-01-01 00:00:00";
			}

			if(!StringUtils.isEmpty(end)) {
				end+= " 23:59:59";
			}else {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				end = sdf.format(new Date());
			}
			List<UserMessage> messages = new UserMessageDao().select(connection,LIMIT_NUM,id,start,end);
			commit(connection);

			return messages;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	//特定の質問1つを表示するメソッド
	public Message select(int messageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			Message message = new MessageDao().select(connection,messageId);
			commit(connection);
			return message;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}
	}

	//指定のつぶやきを削除するメソッド
	public void delete(int messageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection , messageId);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	//つぶやきの編集をするメソッド
	public void update(Message message) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().upDate(connection, message.getText(), message.getId());
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
